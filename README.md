# AP

Dieses Dokument soll euch bei der Durchführung des Anfängerpraktikums helfen. Es wird ständig aktualisiert und basiert auf meinen Erfahrungen als Tutor. Wenn ihr merkt, dass etwas Neues dazu gekommen ist, das ihr bei eurer Vorbereitung, Durchführung und Auswertung noch nicht auf dem Schirm hattet, ist das nicht schlimm. Ich werde nur das in eure Bewertung einfließen lassen, was bis zum bzw. am Versuchstag kommuniziert wurde.


## Wichtige Hinweise
-   Eine **sehr gute Vorbereitung** ist essentiell für das Durchführen jedes Experiments. In den verschiedenen Ordnern findet ihr Zusatzmaterial, das euch beim Experimentieren und Auswerten helfen soll.
-   Verwendet bitte das **Musterprotokoll**, das ihr auf ILIAS findet, um euer eigenes Versuchsprotokoll anzufertigen. Die **Anhaltspunkte zur Benotung** findet ihr ebenfalls auf ILIAS.
-   Bitte gebt zu allem, das nicht von euch selbst stammt, eine **Quelle** an.
-   Nicht nur die verwendeten Messgeräte müssen dokumentiert werden, sondern auch die verwendete **Software** (mit Versionsnummer).
-   **Bei Fragen - fragen!** Ich stehe euch gerne per Mail zur Verfügung, wenn ihr Fragen zur Vorbereitung, Durchführung und Auswertung von eurem Experiment habt.


## Das Laborbuch
-   Verwendet bitte **keinen Bleistift im Laborbuch**. Wenn ihr einen Fehler gemacht habt, streicht ihn bitte sauber durch. Wenn ihr eine Messreihe neu aufnehmen müsst, macht bitte eine entsprechende Notiz.
-   Falls ihr Versuchsteile **nicht vollständig** erledigen konntet, notiert das bitte ebenfalls und begründet es.
-   Es werden keine Laborbücher ausgegeben. Erfahrungsgemäß ist ein **kariertes DIN A4 Heft mit kariertem Rand als Laborbuch** sehr geeignet. Bitte bringt ein entsprechendes Laborbuch mit und hängt es als Scan eurem Protokoll an. Im 1. OG des Praktikumsgebäudes findet ihr einen Scanner, der auf USB-Sticks (mit FAT Fileformat) speichern kann.
-   Ein **strukturiert geführtes Laborbuch** erspart euch beim Auswerten sehr viel Arbeit, unterschätzt das bitte nicht.
-   Was gehört alles in das Laborbuch?
    -   Versuchsnummer und -titel
    -   Datum der Versuchsdurchführung
    -   Der jeweilige Versuchsteil
    -   Schaltkreise und Skizzen
    -   Beobachtungen
    -   Messdaten: bitte **immer** euren Messwert und die entsprechenden Ungenauigkeiten angeben. Die Art der Ungenauigkeit sollte zudem angegeben werden. Wenn eine Ungenauigkeit in % angegeben wird, notiert das bitte genau so und berechnet den entsprechenden Wert erst bei der Auswertung. Wenn ihr die systematische Messungenauigkeit nicht bei jedem einzelnen Messwert angeben möchtet, weil ihr etwa den selben Skalenbereich und damit eine unveränderte systematische Messungenauigkeit bei jedem Messpunkt habt, könnt ihr die systematische Messungenauigkeit auch für eine gesamte Messtabelle angeben: _σ_<sub>U, syst</sub> = (2% · _U_ + 0.03)/<img src="src/sqrt(3).svg"> V.
        -   Bsp.: 2% + 3 Digits Messungenauigkeit eines Multimeters (angegeben als Toleranz) bei einer Spannungsmessung und einem zusätzlich schwankenden Messwert um 1 Digit (Digit = letzte angezeigte Ziffer): _U_ = (9.820 ± ((2% · _U_ + 0.030)/<img src="src/sqrt(3).svg">)<sub>syst</sub> ± 0.010<sub>stat</sub>) V. Die Kombination von systematischer und statistischer Unsicherheit bestimmt ihr durch die Wurzel der Quadratsumme. Im Protokoll gebt ihr dann folgenden Wert an: _U_ = (9.820 ± 0.131<sub>syst</sub> ± 0.010<sub>stat</sub>) V oder _U_ = (9.82 ± 0.13) V.
    -   Verwendete Messgeräte und Bauteile sowie deren Kenngrößen. Bitte achte darauf, dass eine angegebene Toleranz keine Standardabweichung ist.
 
## Fehlerrechnung
-   Bitte führt alle von euch verwendeten Formeln an.
-   Beim Fitten wird im AP nicht darauf geachtet, ob ihr x-Fehler mit einbezieht. Wenn ihr wisst, wie das geht, steht dem aber nichts entgegen.
-   Ausgleichsgeraden bitte per Hand einzeichnen und zur Fehlerabschätzung extreme Ausgleichsgeraden als Konfidenzintervalle einzeichnen (gestrichelt). Falls ihr das schon mal gemacht habt, dürft ihr gerne Tools dafür verwenden, mit denen ihr gut umgehen könnt. Bitte verzichtet auf den Einsatz von Excel zum Einzeichnen solcher Geraden.
