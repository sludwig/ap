# FAQ

1.  Q: Wir haben lediglich einen Widerstand von den 12 mittels Ohm-Funktion gemessen, weil du meinest, einen Probemessung würde reichen. Wir wissen jetzt nicht ob wir die Bestimmung des Mittelwertes bei der Auswertung einfach auslassen sollen.

    A: Wenn ihr nur einen Widerstand gemessen habt, könnt ihr keinen Mittelwert bestimmen. Es wäre gut, wenn ihr im Protokoll vermerken könntet, dass ihr das aus Zeitgründen nicht gemacht habt. Vergleicht aber trotzdem den gemessenen Widerstand mit der Herstellerangabe (Farbcode) und schätzt eure Unsicherheiten auf die Widerstandsmessung am Würfel ab.


2.  Q: Sind die Toleranzen der Messgeräte jetzt systematische Abweichungen?
    
    A: Toleranzen zählen zu den systematischen Unsicherheiten. Die Unsicherheit wird nicht kleiner je öfter ihr messt, da sie bauartlich bedingt ist.
    
    
3.  Q: Wie sollen jetzt letztlich systematische Abweichungen mit den statistischen Abweichungen verrechnet werden?

    A: In der Auswertung würde ich gerne sehen, dass ihr euch darüber Gedanken gemacht habt, welche Unsicherheiten dominieren. Wenn ihr systematische und statistische Unsicherheiten kombinieren wollt, addiert ihr deren Quadrate unter der Wurzel. Bitte achtet darauf, dass eine Toleranz keine Standardabweichung ist.

4.  Q: Kannst du mir nochmal die Formel, die du an die Tafel geschrieben hast, bezüglich der Unsicherheit erklären? Warum muss für Spannung und Strom auch durch Wurzel 3 geteilt werden?

    A: Bei den systematischen Unsicherheiten, die auf den Datenblätter angegeben sind, handelt es sich um sog. Toleranzen. Eine Toleranz ist in dem Fall eine Herstellerangabe und bezeichnet eine maximal zu erwartende systematische Messunsicherheit. Um mit diesen Messunsicherheiten rechnen zu können (Fehlerfortpflanzung), ist es hilfreich diese maximalen Unsicherheiten als Standardabweichung oder Standardfehler auszudrücken.
    
    Hierzu verwenden wir einen Trick: Die Standardabweichung ist definiert als die Wurzel der Varianz. Und eine Varianz lässt sich für jede Wahrscheinlichkeitsdichtefunktion bestimmen. In unserem Fall nehmen wir der Einfachheit halber eine Gleichverteilung an. Die tatsächliche Verteilung schaut jedoch grundlegend anders aus. Um diese zu bestimmen, müssten wir hinreichend viele Widerstände / Multimeter vermessen, was den zeitlichen Rahmen des Experiments sprengen würde. Also reicht uns für's Erste die Annahme einer Gleichverteilung. Wenn man das Integral zur Berechnung der Varianz löst, bekommt man den Wert a/sqrt(3) für die Standardabweichung. a ist hierbei die Toleranz, die vom Hersteller angegeben wird.
    Für Spannung und Strom, die mit einem Multimeter gemessen werden, teilt ihr die Toleranz (Prozentwert + Digits) durch sqrt(3) und drückt damit die Toleranz als Standardabweichung aus.
    
    Wenn ihr systematische und statistische Messunsicherheit kombinieren wollt, könnt ihr beide Werte quadriert unter der Wurzel addieren. Ihr könnt aber auch die Messunsicherheiten einzeln fortpflanzen. Es ist immer ratsam beide Messunsicherheiten getrennt anzugeben, wenn es möglich ist.
    
5.  Q: Es war gemessener Wert +- syst. Wert (abgelesen aus dem Datenblatt) +- geschätzter Wert (stat)? Für alle gemessenen Werte, egal ob wir Spannung, Strom oder Widerstand direkt gemessen haben, oder?

    A: Der "geschätzte Wert (stat)", den du ansprichst, meint vermutlich den Digit-Flip, den ihr am Digital-Multimeter beobachten könnt (dann ist die statistische Messunsicherheit gleich der Anzahl an Digits, die sich verändert) oder die systematische Messunsicherheit am analogen Multimeter (den müsst ihr abschätzen, meist eignet sich ein halber Skalenwert).

6.  Q: Ist der Skalenendwert schon ein Ablesefehler oder muss man beim analogen Messgerät beide berücksichtigen? Ist der Skalenendwert ein syst. Fehler?

    A: Beim analogen Messgerät müssen sowohl Toleranz als auch Ablesefehler berücksichtigt werden. Um herauszufinden, ob es sich bei einer Unsicherheit um eine statistische oder um eine systematische Messunsicherheit handelt, könnt ihr euch fragen, ob sich die Unsicherheit durch mehrmaliges Messen verringert. Bei einem Digit-Flip am Digital-Multimeter ist das der Fall, da es sich vermutlich um einen Rundungsfehler handelt. Beim Ablesen vom analogen Multimeter shiftet ihr eure Messwertverteilung um einen gewissen Wert wenn ihr die skala schräg ablest, auch wenn ihr den Paralaxenfehler durch den Spiegel verringert. Das wäre dann als systematische Unsicherheit einzuordnen. Wenn sich der Zeiger bewegt, handelt es sich um eine statistische Abweichung.
